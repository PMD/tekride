//
//  AppDelegate.h
//  SelfRetain
//
//  Created by tekride on 07/02/15.
//  Copyright (c) 2015 tekride. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end


//
//  VictimViewController.m
//  SelfRetain
//
//  Created by tekride on 07/02/15.
//  Copyright (c) 2015 tekride. All rights reserved.
//

#import "VictimViewController.h"

@implementation VictimViewController
-(void)dealloc{
    
    NSLog(@"Destroyer");
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //Part1 -creates a strong reference cycle
//    self.block = ^{
//        [self name];    // capturing a strong reference to self
//        // creates a strong reference cycle
//    };
    
    //Part2 -do not create a strong reference cycle
    typeof(self)  weakSelf = self;
    self.block = ^{
        [weakSelf name];    // capturing a strong reference to self
        // creates a strong reference cycle
    };
}

-(void )name{
    
    NSLog(@"tekride");
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
@end

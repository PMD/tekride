//
//  VictimViewController.h
//  SelfRetain
//
//  Created by tekride on 07/02/15.
//  Copyright (c) 2015 tekride. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VictimViewController : UIViewController
@property (copy) void (^block)(void);
@end
